import courseData from "../data/courseData"
import CourseCard from '../components/CourseCard'
import { useEffect, useState} from "react"


export default function Courses(){
	console.log(courseData);
	console.log(courseData[0]);

	const [Allcourses, setAllCourses] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
		})
	}, []);
	const courses = courseData.map(course => {
		return(
            <CourseCard key={course.id} courseProp={course}/>
        );
	});
	return(
		<>
			<h1>Courses</h1>
			{courses}
		</>
    );
}