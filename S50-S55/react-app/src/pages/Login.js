import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from "../UserContext"
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2"

export default function Login() {
    const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);

    useEffect(() => {
	    if(email !== '' && password !== ''){
	        setIsActive(true);
	    }else{
	        setIsActive(false);
	    }
    }, [email, password]);

    function authenticate(e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                "Content-type" : "application/json"
            },
            body: JSON.stringify({
                email : email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);
            if(typeof data.access !== "undefined"){
                localStorage.setItem("token", data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                  icon: 'success',
                  title: 'Login Successful!',
                  text: 'Welcome to Zuitt!',
                })

            }else{
                Swal.fire({
                  icon: 'error',
                  title: 'Authentication Failed!',
                  text: 'Please try again!',
                })
            }
        })

        setEmail('');
        setPassword('');
    }
    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin,
                email: data.email
            })
        })
    }

    return (
        (user.token !== null) ?
            <Navigate to="/courses"/>
        :
        <>
            <h1>Login Page</h1>
            <Form onSubmit={(e) => authenticate(e)} className="my-5">
                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email" 
                        required
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                    />
                </Form.Group>
                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password" 
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                </Form.Group>

                { isActive ? 
                    <Button variant="success" type="submit" id="submitBtn" className="mt-3">
                        Login
                    </Button>
                    : 
                    <Button variant="danger" type="submit" id="submitBtn" className="mt-3" disabled>
                        Login
                    </Button>
                }
            </Form>
        </>
    );
}
