import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext  } from "react";
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

export default function Register(){

	const {user} = useContext(UserContext);
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	useEffect(() => {
		if((email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[email, password1, password2]);

	function registerUser(e) {
		
		e.preventDefault();
		setEmail("");
		setPassword1("");
		setPassword2("");

		alert("Thank you for registering!");
	}

	return(
		(user.token !== null) ?
			<Navigate to="/courses" />
		:
		<>
			<h1>Registration Page</h1>
			<Form className="my-5" onSubmit={e => registerUser(e)}>
				<Form.Group className="mb-3" controlId="userEmail">
					<Form.Label>Email address</Form.Label>
					<Form.Control type="email" value={email} placeholder="Enter email" onChange={e => setEmail(e.target.value)} required/>
					<Form.Text className="text-muted">
					We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>
				<Form.Group className="mb-3" controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" value={password1} placeholder="Password" onChange={e => setPassword1(e.target.value)} required/>
				</Form.Group>
				<Form.Group className="mb-3" controlId="password2">
					<Form.Label>Confirm Password</Form.Label>
					<Form.Control type="password" value={password2} placeholder="Confirm Password" onChange={e => setPassword2(e.target.value)} required/>
				</Form.Group>

				{ isActive ?
					<Button variant="primary" type="submit" id="submitBtn">
						Register
					</Button>
					:
					<Button variant="danger" type="submit" id="submitBtn" disabled>
						Register
					</Button>
				}
		    </Form>
		</>
	
		);
}