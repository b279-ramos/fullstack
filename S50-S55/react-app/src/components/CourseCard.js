import { Card, Button } from 'react-bootstrap';
import { useState } from "react"

export default function CourseCard({courseProp}) {

    console.log(courseProp.name);
    console.log(typeof courseProp);

    const { name, description, price } = courseProp;
    const [count, setCount] = useState(0);
    const [seat, setSeat] = useState(30);

    console.log(useState(0));

    function enroll(){
        if(seat > 0){
            setCount(count + 1);
            setSeat(seat - 1);
        }else{
            alert("No more seats available!");
        }
        
        console.log("Enrollees: " + count);
        console.log("Available Seats: " + seat);
    }

    return (
        <Card className="my-3">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Card.Subtitle>Enrollees:</Card.Subtitle>
                <Card.Text>{count}</Card.Text>
                <Button variant="primary" onClick={enroll}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}