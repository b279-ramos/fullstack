import {Nav, Navbar} from "react-bootstrap"
import { Link, NavLink } from "react-router-dom";
import { useState, useContext, useEffect } from "react"
import UserContext from "../UserContext"

export default function AppNavbar(){

  const { user } = useContext(UserContext);

	return(
    <Navbar bg="light" expand="lg" className="px-3">
      <Navbar.Brand as={Link} to={"/"} className="fw-bold">Zuitt</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ms-auto">
          <Nav.Link as={NavLink} to={"/"}>Home</Nav.Link>
          <Nav.Link as={NavLink} to={"/courses"}>Courses</Nav.Link>
          {
            (user.token !== null) ?
            <Nav.Link as={NavLink} to={"/logout"}>Logout</Nav.Link>
          :
          <>
            <Nav.Link as={NavLink} to={"/register"}>Register</Nav.Link>
            <Nav.Link as={NavLink} to={"/login"}>Login</Nav.Link>
          </>
          }
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}