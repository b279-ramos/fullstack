import './App.css';
import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home"
import Courses from "./pages/Courses"
import Register from "./pages/Register"
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from "./pages/Error"

import { Route, Routes } from "react-router-dom"
import { BrowserRouter as Router } from "react-router-dom"
import { Container } from "react-bootstrap";
import { useState, useEffect } from "react"
import { UserProvider } from "./UserContext"

function App() {

  const [user, setUser] = useState({
      id: null,
      isAdmin: null,
      email: null,
      token: localStorage.getItem("token")
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
          <Container>
              <Routes>
                  <Route path="/" element={<Home/>}/>
                  <Route path="/courses" element={<Courses/>}/>
                  <Route path="/register" element={<Register/>}/>
                  <Route path="/login" element={<Login/>}/>
                  <Route path="/logout" element={<Logout/>}/>
                  <Route path="*" element={<Error/>}/>
              </Routes>
          </Container>
      </Router>
    </UserProvider>
  );
}

export default App;